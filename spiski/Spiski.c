#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h>
#include <stdlib.h>
typedef struct List{
    int dat;
    struct List* next;
}List;
typedef struct Tree{
    int dat;
    struct Tree* left;
    struct Tree* right;
}Tree;
List* Push_List(List** l,int dat){
   List* cur=*l;
   List* p=(List*)malloc(sizeof(List));
   p->dat=dat;
   p->next=NULL;
   if(cur==NULL){
      *l=p;
   }
   else{
      while(cur->next!=NULL){
          cur=cur->next;
      }
      cur->next=p;
   }
}
void print_List(const List* l){
    for(;l;l=l->next)
    	printf("%d ",l->dat);
    printf("\n");
}
void Bubble_Sorted_List(List* l){
	List* start=NULL;
	int i=1;
	while(i!=0){
		i=0;
		start=l;
		while(start->next!=NULL){
			if(start->dat>(start->next)->dat){
				i=start->dat;
				start->dat=(start->next)->dat;
				(start->next)->dat=i;
				i=1;
			}
			start=start->next;
		}
	}
}
Tree* newNode(int dat){
    Tree* t=(Tree*)malloc(sizeof(Tree));
    t->dat=dat;
    t->left=NULL;
    t->right=NULL; 
    return t;
}
List* Build_AVL_Recursive(List **tr,int r){
    if(r<=0)
    	return NULL;
    Tree *left=Build_AVL_Recursive(tr, r/2);
    Tree *m=newNode((*tr)->dat);
    m->left=left;
    *tr=(*tr)->next;
    m->right=Build_AVL_Recursive(tr, r-r/2-1);
    return m;
}
int sch_List(List *l){
    int sch=0;
    List *tmp=l;
    while(tmp){
        tmp=tmp->next;
        sch++;
    }
    return sch;
}
Tree* Build_AVL(List* l){
	List* tmp=l;
    int r=sch_List(tmp);
    return Build_AVL_Recursive(&tmp,r);
}
void Print_Tree(Tree* tr,int lvl){
	int i;
    if(tr){
        Print_Tree(tr->right,lvl+1);
        for(i=0;i<lvl;i++){
            printf("     ");
        }  
        printf("%d\n\n", tr->dat);
        Print_Tree(tr->left, lvl+1);
    }
}
int main(int argc,char* argv[]){
	FILE* f;
	int celoe;
	if(argc<2){
		printf("An input command line argument");
		return -1;
	}
	f=fopen(argv[1],"r");
	if(f==NULL){
		printf("Error open file\n");
		return -1;
	}
	List* l=NULL;
	while(feof(f)==0){
		fscanf(f,"%i",&celoe);
		Push_List(&l,celoe);
	}
	printf("Initial list:\n");
    print_List(l);
    Bubble_Sorted_List(l);
    printf("Sorted list:\n");
    print_List(l);
    struct Tree *m=Build_AVL(l);
    printf("\nTree: \n");
    Print_Tree(m,0);
    fclose(f);
	return 0;
}