﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 200
#define M 30
typedef struct List{
	struct List *next;
	char surname[M];
	char name[M];	
	char patronymic[M];
	char number[M];
}List;
unsigned int keygen(char* surname){
	unsigned int key;
		for(key=0;*surname!='\0';surname++)
			key=*surname+10*key;
		return key%N;
}
int main(int argc,char* argv[]){
	if(argc<2){
		printf("An input command line argument");
		return 0;
	}
	FILE *f;
	int i=0,j=0,mesto,p;
	List mass[N]={0};
	char a[N],skanf[M];
	f=fopen(argv[1],"r");
	if (f==NULL) {
		printf("Error open file");
		return 0;
	}
	List* dop_struct=NULL;
	while(!feof(f)){
		fgets(a,N,f);
		i=0;
		j=0;
		char name_dat[M];
		char surname_dat[M];
		char patronymic_dat[M];
		char number_dat[M];
		while(a[i]!='\t'){
			surname_dat[j]=a[i];
			i++;
			j++;
		}
		surname_dat[j]='\0';
		mesto=keygen(surname_dat);
		i++;
		j=0;
		while(a[i]!='\t'){
			name_dat[j]=a[i];
			i++;
			j++;
		}
		name_dat[j]='\0';
		i++;
		j=0;
		while(a[i]!='\t'){
			patronymic_dat[j]=a[i];
			i++;
			j++;
		}
		patronymic_dat[j]='\0';
		i++;
		j = 0;
		while(a[i]!='\n'&&a[i]!='\0'){
			number_dat[j]=a[i];
			i++;
			j++;
		}
		number_dat[j]='\0';
		if(mass[mesto].surname[0]=='\0'){
			strcpy(mass[mesto].surname,surname_dat);
			strcpy(mass[mesto].name,name_dat);
			strcpy(mass[mesto].patronymic,patronymic_dat);
			strcpy(mass[mesto].number,number_dat);
		}
		else{
			dop_struct=mass[mesto].next;
			while(dop_struct!=NULL){
				dop_struct=dop_struct->next;
			}
			dop_struct=(List*)malloc(sizeof(List));
			strcpy(dop_struct->surname,surname_dat);
			strcpy(dop_struct->name,name_dat);
			strcpy(dop_struct->patronymic,patronymic_dat);
			strcpy(dop_struct->number,number_dat);
		}	
	}
	fclose(f);
	while(1){
		printf("Write surname\n");
		gets(skanf);
		if(skanf[0]=='\0'){
			gets(skanf);
			if(skanf[0]=='\0'){
				break;
			}
		}
		mesto=keygen(skanf);
		p=1;
		if(mass[mesto].surname[0]=='\0'){
			printf("Such surnume not on the list\n");
		}
		else{
			dop_struct=&mass[mesto];
			while(strcmp(dop_struct->surname,skanf)!=0){
				if(dop_struct->next==NULL&&strcmp(dop_struct->surname, skanf)!=0){
					printf("Such surnume not on the list\n");
					p=0;
					break;
				}
				dop_struct=dop_struct->next; 
			}
			if(p==1){
				printf("%s\n",dop_struct->surname);
				printf("%s\n",dop_struct->name);
				printf("%s\n",dop_struct->patronymic);
				printf("%s\n",dop_struct->number); 
			}
		}
	}
	return 0;
} 