#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 300
int n=1,m=1,len_MAX=0;
void files_create(char** massukaz,int sch,int len,int tmp1){
	int i,l=0;
	FILE* f;
	char file_name[8];
	if(len>len_MAX)
		len_MAX=len;
	sprintf(file_name,"t%i.txt",n);
	n++;
	f=fopen(file_name,"wb");
	if(f==NULL){
		printf("Error open file 'create'\n");
		return;
	}
	for(i=0;i<sch;i++){
		fputs(massukaz[i],f);
		fputs("\n",f);
	}
	fclose(f);
}
int line_counter(char* a){
	int i,sch=0;
	for(i=0;i<N;i++)
		if(a[i]=='\n')
			sch++;
	return sch;			
}
int sort_mass(char* a,int sch,int M){
	int i,j=0,k,l=0,h=0,tmp1=0,tmp2=0,len=0,sum_tub=0,sum_tab=0;
	char* mass=NULL;
	char** massukaz=NULL;
	char* yach1=NULL;
	char* yach2=NULL;
	char* str1=NULL;
	for(i=0;i<N;i++){
		if(a[i]=='\n'){
			tmp2=i-tmp1+2;
			tmp1=i+1;
		}
		if(len<tmp2)
			len=tmp2;
	}
	mass=(char*)malloc(len*sch);
	if(mass==NULL){
		printf("memory error 'mass'");
		return -1;
	}
	massukaz=(char**)malloc(len);
	if(massukaz==NULL){
		printf("memory error 'massukaz'");
		return -1;
	}
	for(i=0;i<sch;i++)
		massukaz[i]=&mass[i*len];
	memset(mass,0,len*sch);		
	for(i=0;i<tmp1;i++){
		if(a[i]=='\n'){
			massukaz[j][l]=a[i];
			massukaz[j][l+1]='\0';
			j++;
			l=0;
		}
		else{
			massukaz[j][l]=a[i];
			l++;
		}
	}
	yach1=(char*)malloc(len);
	if(yach1==NULL){
		printf("memory error 'yach1'");
		return -1;
	}
	memset(yach1,0,len);
	yach2=(char*)malloc(len);
	if(yach2==NULL){
		printf("memory error 'yach2'");
		return -1;
	}
	memset(yach2,0,len);
	str1=(char*)malloc(len);
	if(str1==NULL){
		printf("memory error 'str1'");
		return -1;
	}
	memset(str1,0,len);
	l=0;
	while(l<sch){
		for(k=1+l;k<sch;k++){
			for(i=0;i<len;i++){
				if((sum_tub==M-1)&&massukaz[l][i]!='\t'){
					yach1[j]=massukaz[l][i];
					j++;
				}
				if((sum_tab==M-1)&&massukaz[k][i]!='\t'){
					yach2[h]=massukaz[k][i];
					h++;
				}
				if(massukaz[l][i]=='\t')
					sum_tub++;
				if(massukaz[k][i]=='\t')
					sum_tab++;
			}
			if(strcmp(yach1,yach2)>0){				
				memcpy(str1,massukaz[l],len);
				memset(massukaz[l],0,len);
				memcpy(massukaz[l],massukaz[k],len);
				memset(massukaz[k],0,len);
				memcpy(massukaz[k],str1,len);
				memset(str1,0,len);
			}
			memset(yach1,0,len);
			memset(yach2,0,len);
			sum_tub=0;
			sum_tab=0;
			h=0;
			j=0;
		}
		l++;
	}
	files_create(massukaz,sch,len,tmp1);
	memset(mass,0,len*sch);
	memset(str1,0,len);
	for(i=0;i<sch;i++)
		free(massukaz[i]);	
	free(mass);
	free(yach1);
	free(yach2);
	free(str1);
	return tmp1;
}
void clear(int m){
	char file_name[8];
	sprintf(file_name,"t%i.txt",m);
	remove(file_name);
}
int merge_file(int M,char* argv[]){
	int i,j=0,sum_tub=0,bias1=0,bias2=0,tmp;
	FILE* f1;
	FILE* f2;
	FILE* f_new;
	char* yach1=(char*)malloc(len_MAX);
	if(yach1==NULL){
		printf("memory error 'yach1'");
		return -1;
	}
	memset(yach1,0,len_MAX);
	char* yach2=(char*)malloc(len_MAX);
	if(yach2==NULL){
		printf("memory error 'yach2'");
		return -1;
	}
	memset(yach2,0,len_MAX);
	char* str1=(char*)malloc(len_MAX);
	if(str1==NULL){
		printf("memory error 'str1'");
		return -1;
	}
	memset(str1,0,len_MAX);
	char* str2=(char*)malloc(len_MAX);
	if(str2==NULL){
		printf("memory error 'str2'");
		return -1;
	}
	memset(str2,0,len_MAX);
	char file_open[8],file_name[8];
	tmp=n;
	while(m<tmp){
		if(m<tmp&&n-2>m){
			sprintf(file_open,"t%i.txt",m);
			m++;
			f1=fopen(file_open,"rb");
			if(f1==NULL){
				printf("Error open file 'merge'\n");
				return -1;
			}
			sprintf(file_open,"t%i.txt",m);
			m++;
			f2=fopen(file_open,"rb");
			if(f2==NULL){
				printf("Error open file 'merge'\n");
				return -1;
			}
			sprintf(file_name,"t%i.txt",n);
			n++;
			f_new=fopen(file_name,"wb");
			if(f_new==NULL){
				printf("Error open file 'merge'\n");
				return -1;
			}
			while(!feof(f1)&&!feof(f2)){
				fgets(str1,len_MAX,f1);
				bias1=strlen(str1);
				for(i=0;i<len_MAX;i++){
					if((sum_tub==M-1)&&str1[i]!='\t'){
						yach1[j]=str1[i];
						j++;
					}
					if(str1[i]=='\t')
						sum_tub++;
				}
				j=0;
				sum_tub=0;
				fgets(str2,len_MAX,f2);
				bias2=strlen(str2);
				for(i=0;i<len_MAX;i++){
					if((sum_tub==M-1)&&str2[i]!='\t'){
						yach2[j]=str2[i];
						j++;
					}
					if(str2[i]=='\t')
						sum_tub++;
				}
				if(strcmp(yach1,yach2)>0){
					fputs(str2,f_new);
					fputs("\n",f_new);
					fseek(f1,-bias1,SEEK_CUR);
				}
				else{
					fputs(str1,f_new);
					fputs("\n",f_new);
					fseek(f2,-bias2,SEEK_CUR);
				}
				memset(yach1,0,len_MAX);
				memset(yach2,0,len_MAX);
				memset(str1,0,len_MAX);
				memset(str2,0,len_MAX);
				sum_tub=0;
				j=0;
			}	
			if(feof(f1)){
				fclose(f1);
				clear(m-2);
				while(!feof(f2)){
					fgets(str2,len_MAX,f2);
					fputs(str2,f_new);
					memset(str2,0,len_MAX);
				}
				fclose(f2);
				clear(m-1);
			}
			else{
				fclose(f2);
				clear(m-1);
				while(!feof(f1)){
					fgets(str1,len_MAX,f1);
					fputs(str1,f_new);
					memset(str1,0,len_MAX);
				}
				fclose(f1);
				clear(m-2);
			}
			fclose(f_new);
		}
		else{
			if(n-2==m){
				sprintf(file_open,"t%i.txt",m);
				m++;
				f1=fopen(file_open,"rb");
				if(f1==NULL){
					printf("Error open file 'merge'\n");
					return -1;
				}
				sprintf(file_open,"t%i.txt",m);
				m++;
				f2=fopen(file_open,"rb");
				if(f2==NULL){
					printf("Error open file 'merge'\n");
					return -1;
				}
				f_new=fopen(argv[3],"wb");
				if(f_new==NULL){
					printf("Error open file 'merge'\n");
					return -1;
				}
				while(!feof(f1)&&!feof(f2)){
					fgets(str1,len_MAX,f1);
					bias1=strlen(str1);
					for(i=0;i<len_MAX;i++){
						if((sum_tub==M-1)&&str1[i]!='\t'){
							yach1[j]=str1[i];
							j++;
						}
						if(str1[i]=='\t')
							sum_tub++;
					}
					j=0;
					sum_tub=0;
					fgets(str2,len_MAX,f2);
					bias2=strlen(str2);
					for(i=0;i<len_MAX;i++){
						if((sum_tub==M-1)&&str2[i]!='\t'){
							yach2[j]=str2[i];
							j++;
						}
						if(str2[i]=='\t')
							sum_tub++;
					}
					if(strcmp(yach1,yach2)>0){
						fputs(str2,f_new);
						fputs("\n",f_new);
						fseek(f1,-bias1,SEEK_CUR);
					}
					else{
						fputs(str1,f_new);
						fputs("\n",f_new);
						fseek(f2,-bias2,SEEK_CUR);
					}
					memset(yach1,0,len_MAX);
					memset(yach2,0,len_MAX);
					memset(str1,0,len_MAX);
					memset(str2,0,len_MAX);
					j=0;
					sum_tub=0;
				}	
				if(feof(f1)){
					fclose(f1);
					clear(m-2);
					while(!feof(f2)){
						fgets(str2,len_MAX,f2);
						fputs(str2,f_new);
						memset(str2,0,len_MAX);
					}
					fclose(f2);
					clear(m-1);
				}
				else{
					fclose(f2);
					clear(m-1);
					while(!feof(f1)){
						fgets(str1,len_MAX,f1);
						fputs(str1,f_new);
						memset(str1,0,len_MAX);
					}
					fclose(f1);
					clear(m-2);
				}
				fclose(f_new);
			}
			return 0;
		}
	}
	if(n-1>m){
		return merge_file(M,argv);
	}
}
int main(int argc, char* argv[]) {
	int bias=0,M,l=0,sch;
	char a[N]={0};
	FILE* fp;
	if (argc<4){
		printf("An input command line argument");
		return -1;
	}
	sscanf(argv[2],"%i",&M);
	fp=fopen(argv[1],"rb");
	if(fp==NULL){
		printf("Error open file\n");
		return -1;
	}
	while(!feof(fp)){
		memset(a,0,N);
		l=fread(a,1,N,fp);
		if(l<N&&!feof(fp))
			printf("With files read less N\n");
		sch=line_counter(a);
		if(sch<1){
			printf("An overly long string in the file. Redact the file and try again");
			return -1;
		}
		bias+=sort_mass(a,sch,M);
		if(feof(fp)){
			printf("End of file\n");
			fclose(fp);
			break;
		}
		fseek(fp,bias,SEEK_SET);
	}
	merge_file(M,argv);
	return 0;
}
