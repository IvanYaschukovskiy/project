#include <stdio.h>
#define N 5 
#define Weight 8
int Table[N][Weight+1];
int Predmet[N][2];
int Maximum(int x,int y){
	if (x>=y) 
		return x;
	else
		return y;
}
void The_output_sequence_of_objects(int size,int quantity){
	if(Table[quantity][size]==0)
		return;
	if(Table[quantity][size]==Table[quantity-1][size]){
		The_output_sequence_of_objects(size,quantity-1);
	}
	else {
		printf("%i",quantity);
		size=size-Predmet[quantity][0];
		The_output_sequence_of_objects(size,quantity-1);
	}

}
int main(){
	int i,j;
	int size=Weight;
	int quantity=N-1;
	Predmet[1][0]=3;
	Predmet[1][1]=5;
	Predmet[2][0]=5;
	Predmet[2][1]=10;
	Predmet[3][0]=4;
	Predmet[3][1]=6;
	Predmet[4][0]=2;
	Predmet[4][1]=5;
	for(i=0;i<=Weight;i++)
		Table[0][i]=0;
	for(i=0;i<N;i++)
		Table[i][0]=0;

	for(i=1;i<N;i++){
		for(j=1;j<=Weight;j++){
			if(j>=Predmet[i][0])
				Table[i][j]=Maximum(Table[i-1][j],Table[i-1][j-Predmet[i][0]]+Predmet[i][1]);
			else
				Table[i][j]=Table[i-1][j];
		}
	}
	for(i=0;i<N;i++){
		for(j=0;j<=Weight;j++){
			printf("%i ",Table[i][j]);
		}
		printf("\n");
	}
	The_output_sequence_of_objects(size,quantity);
	return 0;
}