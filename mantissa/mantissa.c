#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int main() {
	int sdvig = 1023;
	long long int z,e,m,maska_mant = 0xfffffffffffff,maska_exp = 0x7ff;
	long long int *b = NULL;
	double number;
	double *c=NULL;
	double m1;
	while (1) {
		printf("\n");
		printf("Write double ");
		scanf("%lf", &number);
		b = &number;
		z = *b >> 63;
		e = (*b >> 52) & maska_exp;
		m = *b & maska_mant;
		m = m | 0x3fe0000000000000;
		c = &m;
		m1 = *c;
		e = e - sdvig + 1;
		if (z == 0) {
			printf("Znak: +\n");
		}
		else {
			printf("Znak: -\n");
		}
		printf("Exponenta: %lli\nMantissa: %lf ", e, m1);
	}
}
