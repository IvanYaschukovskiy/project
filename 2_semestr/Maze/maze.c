#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#define N 50
#define i_in_c 48
int check(char** maze, int** to_go, int* cur_place, int size)
{
	int i, j;
	for (i = 0;i < 4;i++)
	{
		if (cur_place[0] + to_go[i][0] >= 0 && cur_place[0] + to_go[i][0] < size && cur_place[1] + to_go[i][1] >= 0 && cur_place[1] + to_go[i][1] < size)
		{
			j = maze[cur_place[0] + to_go[i][0]][cur_place[1] + to_go[i][1]];
			if (j != '#' && j != '0' && j != '1' && j != '2' && j != '3' && j != '4')
			{
				return i + 1;
			}
		}
	}
	return -1;
}
void go(char** maze, int* cur_place, int** to_go, int t)
{
	int j;
	if (t == -1)
	{
		j = maze[cur_place[0]][cur_place[1]] - i_in_c;
		maze[cur_place[0]][cur_place[1]] = '0';
		cur_place[0] -= to_go[j - 1][0];
		cur_place[1] -= to_go[j - 1][1];
		return;
	}
	cur_place[0] += to_go[t - 1][0];
	cur_place[1] += to_go[t - 1][1];
	if (maze[cur_place[0]][cur_place[1]] == ' ') 
	{
		maze[cur_place[0]][cur_place[1]] = t + i_in_c;
	}
}
void print(char** maze, int size)
{
	int i, j;
	for (i = 0;i < size;i++)
	{
		for (j = 0;j < size;j++)
		{
			if (maze[i][j] == '0')
			{
				printf("%c", ' ');
			}
			if (maze[i][j] == '1' || maze[i][j] == '2' || maze[i][j] == '3' || maze[i][j] == '4')
			{
				printf("%c", '*');
			}
			if (maze[i][j] == 'A' || maze[i][j] == 'B' || maze[i][j] == '#' || maze[i][j] == ' ')
			{
				printf("%c", maze[i][j]);
			}
		}
		printf("\n");
	}
}
void search_path(char** maze, int* cur_place, int** to_go, int size)
{
	while (maze[cur_place[0]][cur_place[1]] != 'B')
	{
		int t = check(maze, to_go, cur_place, size);
		go(maze, cur_place, to_go, t);
		if (maze[cur_place[0]][cur_place[1]] == 'A')
		{
			printf("Maze has no way\n");
			return;
		}
	}
	print(maze, size);
}
void destroy_matrix(int** to_go, char** maze, int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		free(maze[i]);
	}
	free(maze);
	for (i = 0; i < 4; i++)
	{
		free(to_go[i]);
	}
	free(to_go);
}
int main(int argc, char* argv[]) 
{
	if (argc < 2)
	{
		printf("An input command line argument");
		return -1;
	}
	FILE* f = fopen(argv[1], "r");
	if (f == NULL) 
	{
		printf("Error open file");
		return -1;
	}
	char str[N];
	fgets(str, N, f);
	int size = 0, i, j;
	size = ftell(f);
	if (size == 0)
	{
		printf("Empty file");
		fclose(f);
		return -1;
	}
	fseek(f, 0, SEEK_END);
	j = ftell(f);
	if (j % size != 0)
	{
		printf("Incorrect edit file");
		fclose(f);
		return -1;
	}
	fseek(f, 0, SEEK_SET);
	size -= 2;
	int cur_place[2] = { 0, 0 };
	char** maze = calloc(size, sizeof(char*));
	for (i = 0;i < size;i++)
	{
		maze[i] = calloc(size, 1);
	}
	j = 0;
	while (j != size)
	{
		fgets(str, N, f);
		for (i = 0;i < size;i++)
		{
			maze[j][i] = str[i];
			if (str[i] == 'A')
			{
				cur_place[0] = j;
				cur_place[1] = i;
			}
		}
		j++;
	}
	fclose(f);
	for (i = 0;i < size;i++)
	{
		for (j = 0;j < size;j++)
		{
			printf("%c", maze[i][j]);
		}
		printf("\n");
	}
	int** to_go = calloc(4, sizeof(int*));
	for (i = 0;i < 4;i++)
	{
		to_go[i] = calloc(2, 4);
	}
	to_go[0][0] = -1;
	to_go[1][1] = 1;
	to_go[2][0] = 1;
	to_go[3][1] = -1;
	search_path(maze, cur_place, to_go, size);
	destroy_matrix(to_go, maze, size);
	return 0;
}