#include "Archive_h.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 256
typedef struct tree
{
	int frequency;
	uchar symbol;
	int zero_or_one;
	struct tree* parent;
	struct tree* zero;
	struct tree* one;
}Tree;
uchar** initial_matrix_uchar(int high, int lenght)
{
	int i;
	uchar** mtx = calloc(high, sizeof(uchar*));
	for (i = 0;i < high;i++)
	{
		mtx[i] = calloc(lenght, sizeof(uchar));
	}
	return mtx;
}
int** initial_matrix_int(int high, int lenght)
{
	int i;
	int** mtx = calloc(high, sizeof(int*));
	for (i = 0;i < high;i++)
	{
		mtx[i] = calloc(lenght, sizeof(int));
	}
	return mtx;
}
void destroy_matrix(void** tmp_mtx, int high)
{
	int i;
	for (i = 0;i < high;i++)
	{
		free(tmp_mtx[i]);
	}
	free(tmp_mtx);
}
void destroy_Tree(Tree* node)
{
	if (node->zero == NULL && node->one == NULL)
	{
		return;
	}
	destroy_Tree(node->zero);
	destroy_Tree(node->one);
	free(node);
}
int compare_int(const void* a, const void* b)
{
	return *(int*)b - *(int*)a;
}
void add_node_tree(Tree* mass, Tree* node, int* mass_priority, int i, int count, int left_right)
{
	Tree* tmp = NULL;
	int j;
	for (j = count - 1;j > -1;j--)
	{
		tmp = mass + j;
		while (tmp->parent != NULL)
		{
			tmp = tmp->parent;
		}
		if (tmp->frequency == mass_priority[i - left_right])
		{
			tmp->parent = node;
			if (left_right == 0)
			{
				node->one = tmp;
				tmp->zero_or_one = 1;
			}
			else
			{
				node->zero = tmp;
				tmp->zero_or_one = 0;
			}
			break;
		}
	}
}
Tree* Create_tree(int** matrix, int* mass_priority, int count)
{
	int freq, i = 0;
	Tree* node = NULL;
	Tree* mass = calloc(count, sizeof(Tree));
	for (i = 0;i < count;i++)
	{
		mass[i].frequency = matrix[1][i];
		mass[i].symbol = matrix[0][i];
		mass[i].zero_or_one = -1;
	}
	while (i != 0)
	{
		i = count;
		while (mass_priority[i] == 0)
		{
			i--;
		}
		if (i == 0)
		{
			break;
		}
		freq = mass_priority[i] + mass_priority[i - 1];
		node = calloc(1, sizeof(Tree));
		node->frequency = freq;
		add_node_tree(mass, node, mass_priority, i, count, 0);
		add_node_tree(mass, node, mass_priority, i, count, 1);
		mass_priority[i] = 0;
		mass_priority[i - 1] = freq;
		qsort(mass_priority, count, 4, compare_int);
	}
	return node;
}
uchar** initial_dictionary(Tree* root, uchar** dictionary, int count, int freq_one_symbol)
{
	int i = 2;
	uchar* bufer = malloc((count + 1));
	memset(bufer, 2, (count + 1));
	Tree* tmp = root;
	while (tmp->zero != NULL)
	{
		tmp = tmp->zero;
		bufer[i] = tmp->zero_or_one;
		i++;
	}
	while (tmp->frequency != freq_one_symbol)
	{
		tmp -= 1;
	}
	Tree* list1 = tmp;
	uchar* buf = malloc((count - 1));
	int j = 1, k;
	j = 0;
	while (j != count)
	{
		k = 0;
		memset(bufer, 2, (count + 1));
		tmp = list1;
		tmp = tmp + j;
		bufer[1] = tmp->symbol;
		memset(buf, 2, (count - 1));
		while (tmp->parent != NULL)
		{
			buf[k] = tmp->zero_or_one;
			k++;
			tmp = tmp->parent;
		}
		bufer[0] = k;
		for (i = 2;i < k + 2;i++)
		{			
			bufer[i] = buf[k + 1 - i];
		}
		memcpy(dictionary[j], bufer, (count + 1));
		j++;
	}
	free(buf);
	free(bufer);
	return dictionary;
}
int last_lenght;
uchar tail_code;
void pack_byte(FILE* source_file, int* buf, int lenght)
{
	int i, j, flag = 0;
	uchar byte_buf;
	if (lenght - last_lenght >= 8)
	{
		for (i = 0;i < (lenght + last_lenght) / 8;i++)
		{
			if (last_lenght != 0 && flag != 1)
			{
				flag = 1;
				byte_buf = tail_code;
				for (j = 0;j < 8 - last_lenght;j++)
				{
					if (*(buf + j) == 1)
					{
						byte_buf |= (1 << (7 - last_lenght - j));
					}
				}
				fwrite(&byte_buf, 1, 1, source_file);
				tail_code = 0;
			}
			byte_buf = 0;
			if (last_lenght != 0 && (last_lenght + lenght - 8) >= 8)
			{
				for (j = 0;j < 8;j++)
				{
					if (*(buf + 8 - last_lenght + i * 8 + j) == 1)
					{
						byte_buf |= (1 << (7 - j));
					}
				}
				fwrite(&byte_buf, 1, 1, source_file);
			}
			else
			{
				if (last_lenght == 0)
				{
					for (j = 0;j < 8;j++)
					{
						if (*(buf + i * 8 + j) == 1)
						{
							byte_buf |= (1 << (7 - j));
						}
					}
					fwrite(&byte_buf, 1, 1, source_file);
				}
			}
		}
		tail_code = 0;
		j = 7;
		for (i = (lenght - last_lenght) - lenght % 8;i < lenght;i++)
		{
			if (*(buf + i) == 1)
			{
				tail_code |= (1 << j);
			}
			j--;
		}
	}
	else
	{
		if (last_lenght + lenght >= 8)
		{
			byte_buf = tail_code;
			for (j = 0;j < 8 - last_lenght;j++)
			{
				if (*(buf + j) == 1)
				{
					byte_buf |= (1 << (7 - last_lenght - j));
				}
			}
			fwrite(&byte_buf, 1, 1, source_file);
			byte_buf = 0;
			if (((last_lenght + lenght) / 8) >= 2)
			{
				j = 7;
				for (i = 8 - last_lenght;i < 16 - last_lenght;i++)
				{
					if (*(buf + i) == 1)
					{
						byte_buf |= (1 << j);
					}
					j--;
				}
				fwrite(&byte_buf, 1, 1, source_file);
			}
			tail_code = 0;
			j = 7;
			for (i = lenght - (lenght + last_lenght) % 8;i < lenght;i++)
			{
				if (*(buf + i) == 1)
				{
					tail_code |= (1 << j);
				}
				j--;
			}
		}
		else
		{
			for (j = 0;j < lenght;j++)
			{
				if (*(buf + j) == 1)
				{
					tail_code |= (1 << (7 - last_lenght - j));
				}
			}
		}
	}
	last_lenght = (lenght + last_lenght) % 8;
}
void archiving(uchar** dictionary, char* filename, int count, int kolvo)
{
	FILE* source_file = fopen(filename, "rb");
	if (source_file == NULL)
	{
		printf("Error open file");
		return;
	}
	int i, j;
	char my_file[] = { "huf\0" };
	strncat(filename, my_file, 4);
	FILE* arch_file = fopen(filename, "wb");
	if (arch_file == NULL)
	{
		printf("Error open file");
		return;
	}
	uchar bufer;
	fprintf(arch_file, "%i ", count);
	for (j = 0;j < count;j++)
	{
		fwrite(&dictionary[j][0], 1, 1, arch_file);
		fwrite(&dictionary[j][1], 1, 1, arch_file);
		fwrite(dictionary[j] + 2, 1, dictionary[j][0], arch_file);
	}
	fprintf(arch_file, "%i ", kolvo);
	while (1)
	{
		bufer = fgetc(source_file);
		if (feof(source_file))
		{
			fwrite(&tail_code, 1, 1, arch_file);
			break;
		}
		int j = 0;
		while (dictionary[j][1] != bufer)
		{
			j++;
		}
		int* buf = calloc(dictionary[j][0], sizeof(int));
		i = 2;
		while (dictionary[j][i] != 2)
		{
			buf[i - 2] = dictionary[j][i];
			i++;
		}
		pack_byte(arch_file, buf, dictionary[j][0]);
		free(buf);
	}
	fclose(source_file);
	fclose(arch_file);
}
void control_function(char* filename)
{
	FILE* source_file = fopen(filename, "rb");
	if (source_file == NULL)
	{
		printf("Error open file");
		return;
	}
	uchar bufer;
	if (fscanf(source_file, "%c", &bufer) == EOF)
	{
		printf("Empty file");
		return;
	}
	int kolvo = 1;
	int* table_frequency = calloc(N, sizeof(int));
	while (1)
	{
		table_frequency[bufer]++;
		bufer = fgetc(source_file);
		if (feof(source_file))
		{
			break;
		}
		kolvo++;
	}
	fclose(source_file);
	int i, count = 0;
	for (i = 0;i < N;i++)
	{
		if (table_frequency[i] != 0)
		{
			count++;
		}
	}
	int** matrix = initial_matrix_int(2, count);
	int j = 0;
	for (i = 0;i < N;i++)
	{
		if (table_frequency[i] != 0)
		{
			matrix[0][j] = i;
			matrix[1][j] = table_frequency[i];
			j++;
		}
	}
	free(table_frequency);
	int** matrix_copy = initial_matrix_int(2, count);
	memcpy(matrix_copy[0], matrix[0], count*sizeof(int));
	memcpy(matrix_copy[1], matrix[1], count*sizeof(int));
	qsort(matrix[1], count, 4, compare_int);
	i = 0, j = 0;
	while (i != count)
	{
		while (matrix[1][i] != matrix_copy[1][j] && j != count)
		{
			j++;
		}
		matrix_copy[1][j] = 0;
		matrix[0][i] = matrix_copy[0][j];
		j = 0;
		i++;
	}
	destroy_matrix(matrix_copy, 2);
	int* mass_priority = calloc(count + 1, sizeof(int));
	memcpy(mass_priority, matrix[1], count*sizeof(int));
	Tree* root = Create_tree(matrix, mass_priority, count);
	free(mass_priority);
	uchar** dictionary = initial_matrix_uchar(count, count + 1);
	for (i = 0;i < count;i++)
	{
		memset(dictionary[i], 2, (count + 1));
	}
	dictionary = initial_dictionary(root, dictionary, count, matrix[1][0]);
	Tree* tmp = root;
	while (tmp->frequency != matrix[1][0])
	{
		tmp -= 1;
	}
	destroy_matrix(matrix, 2);
	destroy_Tree(root);
	free(tmp);
	archiving(dictionary, filename, count, kolvo);
	destroy_matrix((void**)dictionary, count);
}