#include "Archive_h.h"
#include <stdio.h>
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("An input command line argument");
		return -1;
	}
	int mode;
	sscanf(argv[2], "%i", &mode);
	if (mode == 1)
	{
		control_function(argv[1]);
	}
	if (mode == 2)
	{
		dearchiving(argv[1]);
	}
	printf("Complete!");
	return 0;
}