#include "Archive_h.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct tree
{
	uchar symbol;
	struct tree* zero;
	struct tree* one;
}tree;
void destroy_tree(tree* node)
{
	if (node == NULL)
	{
		return;
	}
	destroy_tree(node->zero);
	destroy_tree(node->one);
	free(node);
}
tree* Create_Tree(uchar** dictionary, int count)
{
	int i, j;
	tree* root = calloc(1, sizeof(tree));
	tree* tmp = root;
	for (i = 0;i < count;i++)
	{
		tmp = root;
		j = 2;
		while (dictionary[i][j] != 2)
		{
			if (dictionary[i][j] == 0)
			{
				if (tmp->zero == NULL)
				{
					tmp->zero = calloc(1, sizeof(tree));
				}
				tmp = tmp->zero;
			}
			else
			{
				if (tmp->one == NULL)
				{
					tmp->one = calloc(1, sizeof(tree));
				}
				tmp = tmp->one;
			}
			if (dictionary[i][j + 1] == 2)
			{
				tmp->symbol = dictionary[i][1];
			}
			j++;
		}
	}
	return root;
}
void unpack_byte(tree* root, FILE* arch_file, FILE* dearch_file, int count_kolvo_symbol)
{
	uchar bufer;
	tree* tmp = root;
	int i;
	while (count_kolvo_symbol != 0)
	{
		bufer = fgetc(arch_file);
		for (i = 7;i >= 0;i--)
		{
			if ((bufer & (1 << i)) != 0)
			{
				if (tmp->one != NULL)
				{
					tmp = tmp->one;
				}
				else
				{
					fwrite(&tmp->symbol, 1, 1, dearch_file);
					tmp = root;
					tmp = tmp->one;
					count_kolvo_symbol--;
				}
				
			}
			else
			{
				if (tmp->zero != NULL)
				{
					tmp = tmp->zero;
				}
				else
				{
					fwrite(&tmp->symbol, 1, 1, dearch_file);
					tmp = root;
					tmp = tmp->zero;
					count_kolvo_symbol--;
				}
			}
			if (count_kolvo_symbol == 0)
			{
				break;
			}
		}
	}
}
void dearchiving(char* filename)
{
	FILE* arch_file = fopen(filename, "rb");
	int i, j, count_kolvo_symbol, count_alphabet;
	fscanf(arch_file, "%i ", &count_alphabet);
	i = strlen(filename);
	char* my_file = malloc(i - 3);
	memcpy(my_file, filename, i - 4);
	my_file[i - 4] = '\0';
	FILE* dearch_file = fopen(my_file, "wb");
	if (dearch_file == NULL)
	{
		printf("Error open file");
		return;
	}
	free(my_file);
	uchar** dictionary = initial_matrix_uchar(count_alphabet, count_alphabet + 1);
	for (i = 0;i < count_alphabet;i++)
	{
		memset(dictionary[i], 2, (count_alphabet + 1));
	}
	i = 0;
	while (1)
	{
		dictionary[i][0] = fgetc(arch_file);
		dictionary[i][1] = fgetc(arch_file);
		for (j = 2;j < 2 + dictionary[i][0];j++)
		{
			dictionary[i][j] = fgetc(arch_file);
		}
		if (i == count_alphabet - 1)
		{
			break;
		}
		i++;
	}
	fscanf(arch_file, "%i ", &count_kolvo_symbol);
	tree* root = Create_Tree(dictionary, count_alphabet);
	destroy_matrix(dictionary, count_alphabet);
	unpack_byte(root, arch_file, dearch_file, count_kolvo_symbol);
	destroy_tree(root);
	fclose(arch_file);
	fclose(dearch_file);
}