#ifndef graph_lib
#define graph_lib
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
typedef void* graph;
graph create(int size, char type, char weight, char orient);
void add_edge(graph grp, int vertex1, int vertex2, int weight);
graph download_in_file(char* file);
int existence_edge(graph grp, int i, int j);
int size_graph(graph grp);
void remove_edge(graph grp, int vertex1, int vertex2);
void save_in_file(graph grp, char* file);
graph copying_graph(graph grp);
int properties_edge(graph grp, int i, int j);
void destroy(graph grp);
#endif