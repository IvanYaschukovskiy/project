#include "..\..\ConsoleApplication1\ConsoleApplication1\graph_lib.h"
#include <stdlib.h>
int main(int argc, char* argv[])
{
	if (argc < 2) {
		printf("An input command line argument");
		return -1;
	}
	int* p = calloc(5, 4);
	graph g = download_in_file(argv[1]);
	graph grp = copying_graph(g);
	graph gr = copying_graph(p);
	if (gr == NULL)
	{
		printf("NULL\n");
	}
	graph gru = create(6, 's', '0', '0');
	add_edge(gru, 2, 5, 1);
	properties_edge(gru, 5, 2);
	add_edge(g, 2, 6, 5);
	add_edge(grp, 2, 7, 7);
	remove_edge(grp, 1, 6);
	remove_edge(grp, 1, 5);
	save_in_file(g, "graph.txt");
	save_in_file(gru, "graph_tr.txt");
	save_in_file(grp, "graph_copy.txt");
	save_in_file(p, "gph_copy.txt");
	destroy(g);
	destroy(g);
	destroy(grp);
	destroy(p);
	return 0;
}