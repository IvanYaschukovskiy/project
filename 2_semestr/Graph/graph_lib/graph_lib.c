#include "graph_lib.h"
#include <stdio.h>
#include <stdlib.h>
#define M 'm'
#define S 's'
typedef struct list 
{
	int number;
	int weight;
	struct list* next;
}List;
typedef struct Grap 
{
	int size;
	char type;
	char weighting;
	char orient;
	void* pgraph;
}G;
typedef struct Safety 
{
	void* adress;
	struct Safety* next;
}safety;
int** initial_matrix(int size)
{
	int i;
	int** mtx = calloc(size, sizeof(int*));
	for (i = 0;i < size;i++)
	{
		mtx[i] = calloc(size, sizeof(int));
	}
	return mtx;
}
List* initial_list(int size) 
{
	int i;
	List* l_gr=calloc(size,sizeof(List));
	for (i = 0;i < size;i++) 
	{
		l_gr[i].next = calloc(1, sizeof(List));
		l_gr[i].number = -1;
	}
	return l_gr;
}
void destroy_matrix(int** tmp_mtx, int size) 
{
	int i;
	for (i = 0;i < size;i++)
	{
		free(tmp_mtx[i]);
	}
	free(tmp_mtx);
}
void destroy_list(List* tmp_list, int size) 
{
	int i;
	for (i = 0;i < size;i++) 
	{
		List* t = tmp_list[i].next;
		List* r = t;
		while (t->next != NULL) 
		{
			r = r->next;
			free(t);
			t = r;
		}
		free(t);
	}
}
safety* list = NULL;
void add_adress_in_list(graph grp) 
{
	safety* temp = list;
	if (temp == NULL)
	{
		temp = calloc(1, sizeof(safety));
		temp->adress = grp;
		list = temp;
		return;
	}
	while (temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = calloc(1, sizeof(safety));
	temp = temp->next;
	temp->adress = grp;
}
void delete_adress_in_list(graph grp) 
{
	safety* temp1 = list;
	safety* temp2 = temp1;
	if (temp1->next == NULL && temp1->adress == grp) 
	{
		free(temp1);
		list = NULL;
		return;
	}
	if (temp1->adress == grp) 
	{
		list = temp1->next;
		free(temp1);
		return;
	}
	while (temp1->adress != grp) 
	{
		temp2 = temp1;
		temp1 = temp1->next;
	}
	temp2 = temp1->next;
	free(temp1);
}
int check_pointer(graph grp)
{
	safety* temp = list;
	if (temp == NULL)
	{
		return 0;
	}		
	if (temp->next == NULL) 
	{
		if (temp->adress == grp)
		{
			return 1;
		}
	}
	while (temp != NULL) 
	{
		if (temp->adress == grp) 
		{
			return 1;
		}
		temp = temp->next;
	}
	return 0;
}
void add_edge_list(List* l_gr, int vertex1, int vertex2, int weight)
{
	List* t = l_gr[vertex1 - 1].next;
	while (t->next != NULL) 
	{
		t = t->next;
	}
	t->number = vertex2 - 1;
	t->weight = weight;
	t->next = calloc(1, sizeof(List));
	t = t->next;
	t->number = -1;
}
void add_edge(graph grp, int vertex1, int vertex2, int weight) 
{
	if (check_pointer(grp) == 1) 
	{
		G* temp_gr = (G*)grp;
		if (vertex1 > temp_gr->size || vertex1 < 0 || vertex2 > temp_gr->size || vertex2 < 0) 
		{
			printf("Nonexistent vertex number _add");
			return;
		}
		if (temp_gr->type == M) 
		{
			int** tmp_mtx = (int**)temp_gr->pgraph;
			tmp_mtx[vertex1 - 1][vertex2 - 1] = weight;
			if (temp_gr->orient == '0')
			{
				tmp_mtx[vertex2 - 1][vertex1 - 1] = weight;
			}
		}
		if (temp_gr->type == S)
		{
			add_edge_list(temp_gr->pgraph, vertex1, vertex2, weight);
			if (temp_gr->orient == '0')
			{
				add_edge_list(temp_gr->pgraph, vertex2, vertex1, weight);
			}
		}
	}		
}
graph create(int size, char type, char weighting, char orient) 
{
	if (size < 0 || (type != M && type != S) || (weighting != '0' && weighting != '1') || (orient != '0' && orient != '1'))
	{
		printf("Incorrectly data _create");
		return NULL;
	}
	G* grp = calloc(1, sizeof(G));
	add_adress_in_list(grp);
	grp->size = size;
	grp->orient = orient;
	grp->type = type;
	grp->weighting = weighting;
	if (type == M) 
	{
		grp->pgraph = initial_matrix(size);
		if (grp->pgraph == NULL) 
		{
			printf("Error allocating memory");
			return NULL;
		}
		return grp;
	}
	if (type == S) 
	{
		grp->pgraph = initial_list(size);
		if (grp->pgraph == NULL) 
		{
			printf("Error allocating memory");
			return NULL;
		}
		return grp;
	}
	return NULL;
}
graph download_in_file(char* file) 
{
	int vertex1, vertex2, weight, size;
	char type, weighting, orient;
	FILE* f = fopen(file, "r");
	if (f == NULL) 
	{
		printf("Error open file");
		return NULL;
	}
	if (fscanf(f, "%i %c %c %c", &size, &type, &weighting, &orient) == EOF) 
	{
		printf("Empty file");
		return NULL;
	}
	if (size < 0 || (type != M && type != S) || (weighting != '0' && weighting != '1') || (orient != '0' && orient != '1'))
	{
		printf("Incorrectly edit file");
		return NULL;
	}
	G* grp = create(size, type, weighting, orient);
	if (weighting == '1') 
	{
		while (fscanf(f, "%i %i %i", &vertex1, &vertex2, &weight) != EOF)
		{
			if (vertex1 > size && vertex1 < 0 && vertex2 > size && vertex2 < 0)
			{
				printf("File edit incorrectly");
				return NULL;
			}
			add_edge(grp, vertex1, vertex2, weight);
		}
	}
	else 
	{
		while (fscanf(f, "%i %i", &vertex1, &vertex2) != EOF) 
		{
			if (vertex1 > size && vertex1 < 0 && vertex2 > size && vertex2 < 0) 
			{
				printf("File edit incorrectly");
				return NULL;
			}
			add_edge(grp, vertex1, vertex2, 1);
		}
	}		
	fclose(f);
	return grp;
}
int existence_edge(graph grp, int vertex1, int vertex2) 
{
	if (check_pointer(grp) == 1)
	{
		G* temp_gr = (G*)grp;
		if (vertex1 > temp_gr->size || vertex1 < 0 || vertex2 > temp_gr->size || vertex2 < 0)
		{
			printf("Nonexistent vertex number _existence");
			return 0;
		}
		if (temp_gr->type == M) 
		{
			int** tmp_mtx = (int**)temp_gr->pgraph;
			if (tmp_mtx[vertex1 - 1][vertex2 - 1] != 0)
			{
				return 1;
			}
		}
		if (temp_gr->type == S) 
		{
			List* tmp_list = temp_gr->pgraph;
			List* temp = tmp_list[vertex1 - 1].next;
			while (temp->next != NULL)
			{
				if (temp->number == vertex2 - 1) 
				{
					return 1;
				}
				temp = temp->next;
			}
		}
	}	
	return 0;
}
int properties_edge(graph grp, int vertex1, int vertex2)
{
	if (check_pointer(grp) == 1)
	{
		G* temp_gr = (G*)grp;
		if (vertex1 > temp_gr->size || vertex1 < 0 || vertex2 > temp_gr->size || vertex2 < 0)
		{
			printf("Nonexistent vertex number _properties");
			return 0;
		}
		if (existence_edge(grp, vertex1, vertex2) == 0)
		{
			printf("No such edge _properties");
			return 0;
		}
		if (temp_gr->type == M)
		{
			int** tmp_mtx = (int**)temp_gr->pgraph;
			return tmp_mtx[vertex1 - 1][vertex2 - 1];
		}
		if (temp_gr->type == S)
		{
			List* tmp_list = temp_gr->pgraph;
			List* temp = tmp_list[vertex1 - 1].next;
			while (temp->number != vertex2 - 1) 
			{
				temp = temp->next;
			}
			return temp->weight;
		}
	}
	return 0;
}
void save_in_file(graph grp, char* file)
{
	if (check_pointer(grp) == 1)
	{
		G* temp_gr = (G*)grp;
		int vertex1, vertex2;
		FILE* f = fopen(file, "w");
		if (f == NULL) 
		{
			printf("Error open file _save\n");
			return;
		}
		if (temp_gr->weighting == '1') 
		{
			if (temp_gr->orient == '1') 
			{
				fprintf(f, "%i %c %c %c\n", temp_gr->size, temp_gr->type, '1', '1');
				for (vertex1 = 1;vertex1 <= temp_gr->size;vertex1++) 
				{
					for (vertex2 = 1;vertex2 <= temp_gr->size;vertex2++)
					{
						if (existence_edge(grp, vertex1, vertex2)) 
						{
							fprintf(f, "%i %i %i\n", vertex1, vertex2, properties_edge(grp, vertex1, vertex2));
						}
							
					}
				}
			}
			else
			{
				fprintf(f, "%i %c %c %c\n", temp_gr->size, temp_gr->type, '1', '0');
				for (vertex1 = 1;vertex1 <= temp_gr->size;vertex1++)
				{
					for (vertex2 = vertex1;vertex2 <= temp_gr->size;vertex2++)
					{
						if (existence_edge(grp, vertex1, vertex2))
						{
							fprintf(f, "%i %i %i\n", vertex1, vertex2, properties_edge(grp, vertex1, vertex2));
						}
					}
				}
			}
		}
		else 
		{
			if (temp_gr->orient == '1') 
			{
				fprintf(f, "%i %c %c %c\n", temp_gr->size, temp_gr->type, '0', '1');
				for (vertex1 = 1;vertex1 <= temp_gr->size;vertex1++)
				{
					for (vertex2 = 1;vertex2 <= temp_gr->size;vertex2++)
					{
						if (existence_edge(grp, vertex1, vertex2)) 
						{
							fprintf(f, "%i %i\n", vertex1, vertex2);
						}
					}
				}
			}
			else 
			{
				fprintf(f, "%i %c %c %c\n", temp_gr->size, temp_gr->type, '0', '0');
				for (vertex1 = 1;vertex1 <= temp_gr->size;vertex1++)
				{
					for (vertex2 = vertex1;vertex2 <= temp_gr->size;vertex2++)
					{
						if (existence_edge(grp, vertex1, vertex2)) 
						{
							fprintf(f, "%i %i\n", vertex1, vertex2);
						}
					}
				}
			}
		}
		fclose(f);
	}
}
graph copying_graph(graph grp)
{
	if (check_pointer(grp) == 1)
	{
		G* temp_gr = (G*)grp;
		int vertex1, vertex2;
		G* grp_copy = create(temp_gr->size, temp_gr->type, temp_gr->weighting, temp_gr->orient);
		for (vertex1 = 1;vertex1 <= temp_gr->size;vertex1++) 
		{
			for (vertex2 = 1;vertex2 <= temp_gr->size;vertex2++)
			{
				if (existence_edge(temp_gr, vertex1, vertex2)) 
				{
					add_edge(grp_copy, vertex1, vertex2, properties_edge(temp_gr, vertex1, vertex2));
				}
			}
		}
		return grp_copy;
	}
	return NULL;
}
void remove_edge_list(List* l_gr, int vertex1, int vertex2)
{
	List* temp1 = l_gr + vertex1 - 1;
	List* temp2 = temp1;
	while (temp1->number != vertex2 - 1)
	{
		temp2 = temp1;
		temp1 = temp1->next;
	}
	temp2->next = temp1->next;
	free(temp1);
}
void remove_edge(graph grp, int vertex1, int vertex2) {
	if (check_pointer(grp) == 1) 
	{
		G* temp_gr = (G*)grp;
		if (vertex1 > temp_gr->size || vertex1 < 0 || vertex2 > temp_gr->size || vertex2 < 0)
		{
			printf("Nonexistent vertex number _remove");
			return;
		}
		if (existence_edge(grp, vertex1, vertex2) == 0) 
		{
			printf("No such edge _remove");
			return;
		}
		if (temp_gr->type == M) 
		{
			int** tmp_mtx = (int**)temp_gr->pgraph;
			tmp_mtx[vertex1 - 1][vertex2 - 1] = 0;
			if (temp_gr->orient == '0') 
			{
				tmp_mtx[vertex2 - 1][vertex1 - 1] = 0;
			}
		}
		if (temp_gr->type == S)
		{
			List* tmp_list = temp_gr->pgraph;
			remove_edge_list(tmp_list, vertex1, vertex2);
			if (temp_gr->orient == '0')
			{
				remove_edge_list(tmp_list, vertex2, vertex1);
			}
		}
	}	
}
int size_graph(graph grp) 
{
	if (check_pointer(grp) == 1) 
	{
		G* temp_gr = (G*)grp;
		return temp_gr->size;;
	}
	return 0;
}
void destroy(graph grp)
{
	if (check_pointer(grp) == 1)
	{
		delete_adress_in_list(grp);
		G* temp_gr = (G*)grp;
		int size = temp_gr->size;
		if (temp_gr->type == 'm')
		{
			destroy_matrix((int**)temp_gr->pgraph, size);
		}
		if (temp_gr->type == 's') 
		{
			destroy_list(temp_gr->pgraph, size);
		}
		free(temp_gr);
	}
}