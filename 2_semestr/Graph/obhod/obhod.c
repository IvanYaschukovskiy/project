#include "..\..\ConsoleApplication1\ConsoleApplication1\graph_lib.h"
#include <stdio.h>
#include <stdlib.h>
void Depth(Graph g, int size, int node, int* flag) {
	int i;
	flag[node] = 1;
	printf("%i ", node);
	for (i = 1; i <= size;i++) {
		if (existence_edge(g, i, node) && flag[i] != 1) {
			Depth(g, size, i, flag);
		}	
	}
}
void Breadth(Graph g, int size, int* flag) {
	int i,j=1,k=0,l=0;
	int* queue = calloc(size + 1, sizeof(int));
	flag[j] = 1;
	queue[k++] = j;
	while (l < k) {
		j = queue[l++];
		printf("%i ", j);
		for (i = 1;i <= size;i++)
			if (existence_edge(g, j, i) && flag[i] != 1) {
				queue[k++] = i;
				flag[i] = 1;
			}
	}
}
int main(int argc,char* argv[]){
	if (argc < 2) {
		printf("An input command line argument");
		return -1;
	}
	int i;	
	Graph g=download_in_file(argv[1]);
	if(g==NULL)
		return -1;
	int size = size_graph(g);
	int* flag = calloc(size + 1, sizeof(int));
	printf("Depth:\n");
	Depth(g, size, 1, flag);
	for (i = 0;i <= size;i++)
		flag[i] = 0;
	printf("\nBreadth:\n");
	Breadth(g, size, flag);
	Delete_graph_Matrix(g);
	return 0;
}