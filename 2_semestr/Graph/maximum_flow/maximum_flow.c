#include "..\..\ConsoleApplication1\ConsoleApplication1\graph_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 100
int* breadth(graph g, int source, int stok)
{
	int i, j = source, k = 0, l = 0;
	int* flag = calloc(size_graph(g) + 1, sizeof(int));
	int* previous = calloc(size_graph(g) + 1, sizeof(int));
	int* queue = calloc(size_graph(g) + 1, sizeof(int));
	flag[j] = 1;
	previous[j] = 0;
	queue[k++] = j;
	while (l < k)
	{
		j = queue[l++];
		for (i = 1;i <= size_graph(g);i++)
		{
			if (existence_edge(g, j, i) && flag[i] != 1)
			{
				queue[k++] = i;
				flag[i] = 1;
				previous[i] = j;
				if (i == stok)
				{
					return previous;
				}
			}
		}
	}
	free(flag);
	free(queue);
	return previous;
}
void add_edge_reverse(graph g, int min, int i, int j)
{
	if (existence_edge(g, i, j) == 1)
	{
		int weight = properties_edge(g, i, j);
		remove_edge(g, i, j);
		add_edge(g, i, j, min + weight);
	}
	else
	{
		add_edge(g, i, j, min);
	}
}
void search_maximum_flow(graph g, int source, int stok) 
{
	int flow = 0;
	while (1) 
	{
		int* previous = breadth(g, source, stok);
		if (previous[stok] == 0)
		{
			break;
		}
		int i = stok, j, min = N;
		while (i != source)
		{
			j = previous[i];
			if (min > properties_edge(g, j, i)) 
			{
				min = properties_edge(g, j, i);
			}
			i = j;			
		}
		int weight;
		i = stok;
		while (i != source)
		{
			j = previous[i];
			weight = properties_edge(g, j, i);
			add_edge_reverse(g, min, i, j);
			if (weight - min == 0)
			{
				remove_edge(g, j, i);
			}
			else
			{
				remove_edge(g, j, i);
				add_edge(g, j, i, weight - min);
			}
			i = j;
		}
		flow += min;
		min = N;
		i = stok;
		free(previous);
	}
	printf("%i\n", flow);
}
int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("An input command line argument");
		return -1;
	}
	int source, stok;
	while (1) 
	{
		graph g = download_in_file(argv[1]);
		if (g == NULL)
		{
			return -1;
		}
		printf("Write source 1 to %i\n", size_graph(g));
		scanf("%i", &source);
		if (source > size_graph(g) || source < 0)
		{
			printf("Incorrect source. Restart programm and try again\n");
			return -1;
		}
		if (source == 0) 
		{
			break;
		}
		printf("Write stok 1 to %i\n", size_graph(g));
		scanf("%i", &stok);
		if (stok > size_graph(g) || stok < 1)
		{
			printf("Incorrect stok. Restart programm and try again\n");
			return -1;
		}
		search_maximum_flow(g, source, stok);
		destroy(g);
	}
	return 0;
}