#include "..\..\ConsoleApplication1\ConsoleApplication1\graph_lib.h"
#include <stdio.h>
#include <stdlib.h>
#define N 999999
void search_short_path(Graph g, int size, int vertex1, int vertex2) {
	int i, j = vertex1, k = 0, l = 0, m = 0, t = 0;
	int* queue = calloc(size*size, sizeof(int));
	int* path = calloc(size + 1, sizeof(int));
	for (i = 0;i <= size;i++)
		path[i] = N;
	path[j] = 0;
	queue[k++] = j;
	while (l < k) {
		j = queue[l++];
		if (k > size*size-1)
			break;
		for (i = 1;i <= size;i++) {
			if (existence_edge(g, j, i)) {
				queue[k++] = i;
				if (path[i] >= path[j] + properties_edge(g, j, i))
					path[i] = path[j] + properties_edge(g, j, i);
				if (i == vertex2 && path[i] <= N) {
					if (t != path[i])
						t = path[i];
					else
						break;
				}
			}
		}
		if (t == path[i] && i < size)
			break;
	}
	printf("%i\n", path[vertex2]);

}
int main(int argc, char* argv[]) {
	if (argc < 2) {
		printf("An input command line argument");
		return -1;
	}
	Graph g = download_in_file(argv[1]);
	if (g == NULL)
		return -1;
	int size = size_graph(g);
	int vertex1, vertex2;
	while (1) {
		printf("Write the first number of the vertex 1 to %i\n", size);
		scanf("%i", &vertex1);
		if (vertex1 > 6 || vertex1 < 0) {
			printf("Incorrect vertex1. Restart programm and try again\n");
			return -1;
		}
		if (vertex1 == 0)
			break;
		printf("Write the second number of the vertex 1 to %i\n", size);
		scanf("%i", &vertex2);
		if (vertex2 > 6 || vertex2 < 1) {
			printf("Incorrect vertex2. Restart programm and try again\n");
			return -1;
		}
		search_short_path(g, size, vertex1, vertex2);
	}
	Delete_graph_Matrix(g);
	return 0;
}