#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>      		 
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#ifdef _WIN32 
#include <windows.h>
#endif
#define N 120
typedef unsigned char un_c;
un_c* zamena(un_c *str,un_c *substr,un_c *zam){
	int d1=0,d2=0,d3=0;
	int mesto=0,sovp=0,razn=0,count=0,p=0,i,j;
	for(i=sovp+mesto;str[i]!=0;i++){
		if(str[i]==substr[0]){
			mesto=i;
			for(j=i;substr[j-i]!=0;j++){
				if(str[j]==substr[j-i]){
					sovp++;
				}
				else{
					sovp=0;
					break;
				}
			}
		}
		if(sovp==j-mesto){
			while(zam[d3]!=0){
				d3++;
			}
			d2=sovp;
			razn=sovp-d3;
			count++;			
			sovp=0;
		}
	}
	i-=razn*count;
	d1=i;
	un_c *a=(un_c*)malloc((d1+2)*(sizeof(un_c)));
	a[d1+1]='\0';
	j=0,sovp=0;
	for(i=0;str[i]!='\0';i++){
		if(str[i]!=substr[0]){
			a[j]=str[i];
			j++;
		}
		else{
			if(str[i]==substr[0]){
				for(p=i;p<i+d2;p++){
					if(str[p]==substr[p-i]){
						sovp++;
					}
					else break;
				}
			}
			if(sovp==d2){
				sovp=0;
				for(p=i;p<i+d3;p++){
					a[j]=zam[p-i];
					j++;
				}
				i+=d2-1;
			}
			else{
				for(p=i;p<i+sovp;p++){
					a[j]=str[p];
					j++;
				}
				i+=sovp-1;
				sovp=0;
			}
		}
	}
	return a;
}
un_c* capital(un_c *a){
	int i=0;
	while(a[i]!=0){
		i++;
	}
	un_c *b=(un_c*)malloc((i+1)*sizeof(un_c));
	b[i]='\0';
	i=0;
	while(a[i]!='\0'){
		while(a[i]<128&&a[i]!='\0'){
			b[i]=a[i];
			i++;
		}
		if(a[i]>=224&&a[i]<=239)
			b[i]=a[i]-80;
		if(a[i]>=160&&a[i]<=175)
			b[i]=a[i]-32;
		if(a[i]==241)
			b[i]=a[i]-1;
		if(a[i]>=128&&a[i]<160||a[i]==240)
			b[i]=a[i];
		i++;
		if(a[i]=='\0') break;
		while((a[i]!='.'&&a[i]!='?'&&a[i]!='!')&&a[i]!='\0'){
			b[i]=a[i];
			i++;
		}
	}
	return b;
}
un_c* correction(un_c *str){
	int i;
	int s=0,j=0;
	while(str[j]==' '){
		s++;
		j++;
	}
	for(i=j;str[i]!=0;i++){
		if((str[i]=='.'||str[i]==','||str[i]=='?'||str[i]=='!'||str[i]==';'||str[i]==':')&&(str[i+1]!=' ')&&(str[i+1]!='\0'))
			s--;
		if(str[i]==' '&&(str[i+1]=='.'||str[i+1]==','||str[i+1]=='?'||str[i+1]=='!'||str[i+1]==';'||str[i+1]==':'))
			s++;
		if(str[i]==' '&&str[i+1]==' '){
			s++;
			while(str[i+2]==' '){
				s++;
				i++;
			}
		}
	}
	int r=0;
	un_c *a=(un_c*)malloc((i-s+2)*(sizeof(un_c)));
	a[i-s+1]='\0';
	j=0;
	int f=0;
	while(str[j]==' '){
		f++;
		j++;
	}
	for(i=f;str[i]!=0;i++){
		if(str[i]>=128&&str[i]<=242){
			a[r]=str[i];
			r++;
		}
		if(str[i]=='.'||str[i]==','||str[i]=='?'||str[i]=='!'||str[i]==';'||str[i]==':'){
			a[r]=str[i];
			a[r+1]=' ';
			if((a[r]=='.'||a[r]==','||a[r]=='?'||a[r]=='!'||a[r]==';'||a[r]==':')&&(a[r-2]=='.'||a[r-2]==','||a[r-2]=='?'||a[r-2]=='!'||a[r-2]==';'||a[r-2]==':')){
				a[r-1]=a[r];
				a[r]=' ';
				r--;
			}
			r+=2;
		}
		if(str[i]==' '&&(str[i+1]>=128&&str[i+1]<=242)){
			if(a[r-1]!=' '){
				a[r]=str[i];
				r++;
			}
		}		
	}
	return a;
}
un_c* transliteration(un_c *a){
	int r=0,i;
	un_c t[255]={0};
	for(i=0;i<256;i++)
		t[i]=i;		
	t[128]='A';
	t[129]='B';
	t[130]='V';
	t[131]='G';
	t[132]='D';
	t[133]='E';
	t[134]='\0';
	t[135]='Z';
	t[136]='I';
	t[137]='J';
	t[138]='K';
	t[139]='L';
	t[140]='M';
	t[141]='N';
	t[142]='O';
	t[143]='P';
	t[144]='R';
	t[145]='S';
	t[146]='T';
	t[147]='U';
	t[148]='F';
	t[149]='X';
	t[150]='\0';
	t[151]='\0';
	t[152]='\0';
	t[153]='\0';
	t[154]='\0';
	t[155]='\0';
	t[156]='`';
	t[157]='\0';
	t[158]='\0';
	t[159]='\0';
	t[160]='a';
	t[161]='b';
	t[162]='v';
	t[163]='g';
	t[164]='d';
	t[165]='e';
	t[166]='\0';
	t[167]='z';
	t[168]='i';
	t[169]='j';
	t[170]='k';
	t[171]='l';
	t[172]='m';
	t[173]='n';
	t[174]='o';
	t[175]='p';
	t[224]='r';
	t[225]='s';
	t[226]='t';
	t[227]='u';
	t[228]='f';
	t[229]='x';
	t[230]='\0';
	t[231]='\0';
	t[232]='\0';
	t[233]='\0';
	t[234]='\0';
	t[235]='\0';
	t[236]='`';
	t[237]='\0';
	t[238]='\0';
	t[239]='\0';
	t[240]='\0';
	t[241]='\0';
	un_c t2[242][2]={0};
	t2[134][0]='Z';
	t2[134][1]='H';
	t2[150][0]='C';
	t2[150][1]='Z';
	t2[151][0]='C';
	t2[151][1]='H';
	t2[152][0]='S';
	t2[152][1]='H';
	t2[153][0]='\0';
	t2[154][0]='`';
	t2[154][1]='`';
	t2[155][0]='Y';
	t2[155][1]='`';
	t2[157][0]='E';
	t2[157][1]='`';
	t2[158][0]='Y';
	t2[158][1]='U';
	t2[159][0]='Y';
	t2[159][1]='A';
	t2[166][0]='z';
	t2[166][1]='h';
	t2[230][0]='c';
	t2[230][1]='z';
	t2[231][0]='c';
	t2[231][1]='h';
	t2[232][0]='s';
	t2[232][1]='h';
	t2[233][0]='\0';
	t2[234][0]='`';
	t2[234][1]='`';
	t2[235][0]='y';
	t2[235][1]='`';
	t2[237][0]='e';
	t2[237][1]='`';
	t2[238][0]='y';
	t2[238][1]='u';
	t2[239][0]='y';
	t2[239][1]='a';
	t2[240][0]='Y';
	t2[240][1]='O';
	t2[241][0]='y';
	t2[241][1]='o';
	un_c t3[234][2][3]={0};
	t3[153][0][0]='S';
	t3[153][0][1]='H';
	t3[153][0][2]='H';
	t3[233][1][0]='s';
	t3[233][1][1]='h';
	t3[233][1][2]='h';
	for(i=0;a[i]!=0;i++){
		if(t[a[i]]!=0)
			r++;
		else{
			if(t2[a[i]][0]!=0)
				r+=2;
			else r+=3;
		}
	}
	un_c *b=(un_c*)malloc((r+1)*(sizeof(un_c)));
	b[r]='\0';
	int j=0,p=0;
	for(i=0;a[i]!=0;i++){
		if(t[a[i]]!=0){
			b[j]=t[a[i]];
			j++;
		}
		else {
			if(t2[a[i]][0]!=0){
				b[j]=t2[a[i]][0];
				b[j+1]=t2[a[i]][1];
				j+=2;
			}
			else {
				if(a[i]==233){
					p++;
				}
				if(t3[a[i]][p][0]!=0){
					b[j]=t3[a[i]][p][0];
					b[j+1]=t3[a[i]][p][1];
					b[j+2]=t3[a[i]][p][2];
					j+=3;
				}
				p=0;
			}
		}
	}
	return b;	
}
un_c* obr_transliteration(un_c *a){
	int i,j,r,s=0;
	un_c eng1[N]="abvgdezijklmnoprstufxABVGDEZIJKLMNOPRSTUFX``";
	un_c rus1[N]="��������������������倁��������������������";
	un_c eng2[N]="yozhczchsh``y`e`yuyaYOZHCZCHSH``Y`E`YUYA";
	un_c rus2[N]="�������������������";
	for(i=0;a[i]!=0;i++){
		if((a[i]=='s')&&(a[i+1]=='h')&&(a[i+2]=='h')){
			s+=2;
			i+=2;
		}
		if((a[i]=='S')&&(a[i+1]=='H')&&(a[i+2]=='H')){
			s+=2;
			i+=2;
		}
		for(j=0;eng2[j]!=0;j+=2){
			if((a[i]==eng2[j]&&a[i+1]==eng2[j+1])){
				s++;
				i++;
			}
		}
	}
	un_c *b=(un_c*)malloc((i-s+1)*(sizeof(un_c)));
	b[i-s]='\0';
	int k=0,f=0;
	for(i=0;a[i]!=0;i++){
		if((a[i]=='s')&&(a[i+1]=='h')&&(a[i+2]=='h')){
			b[k]=233;
			i+=2;
			k++;
			f++;
		}
		if(f!=1){
			if((a[i]=='S')&&(a[i+1]=='H')&&(a[i+2]=='H')){
				b[k]=153;
				i+=2;
				k++;
				f++;
			}
		}
		if(f!=1){
			for(j=0;j<41;j+=2){
				if (a[i]==eng2[j]&&a[i+1]==eng2[j+1]&&(a[i+2]!='H'||a[i+2]!='h')){
					b[k]=rus2[j/2];
					i++;
					k++;
					f++;
					break;
				}
			}
		}
		if(f!=1){
			for(r=0;r<45;r++){
				if(a[i]==eng1[r]){
					b[k]=rus1[r];
					k++;
					f++;
					break;
				}
			}
		}
		if(f!=1){
			b[k]=a[i];
			k++;
		}		
		f=0;
	}
	return b;
}
int main(){
	un_c str[N]= "  �ਢ�� ,� � � � �. � � ����� .    ��� , �ਢ��  .  . �஫    ��� , �ਢ��  .";
	un_c substr[N]="��� , �ਢ��  .";
	un_c zam[N]="�⫨筮�   ����஥���    !";
	un_c *C=zamena(str,substr,zam);
	printf("%s\n", C);
	un_c *X=capital(str);
	free(C);
	printf("%s\n", X);
	un_c *Y=correction(X);
	free(X);
	printf("%s\n", Y);
	un_c *Z=transliteration(Y);
	free(Y);	
	printf("%s\n", Z);
	un_c *D=obr_transliteration(Z);
	free(Z);
	printf("%s\n", D);
	free(D);	
	return 0;
}